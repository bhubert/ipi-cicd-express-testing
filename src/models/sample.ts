import {
  deleteInTableById,
  getOneFromTableById,
  insertIntoTable,
  queryAsync,
  updateInTableById,
} from '../db';

export interface ISampleDTO {
  name: string
}

export interface ISample {
  id: number
  name: string
}

const sampleModel = {
  async create (payload: ISampleDTO): Promise<ISample> {
    return await insertIntoTable<ISample>('sample', payload);
  },

  async getAll (): Promise<ISample[]> {
    return await queryAsync<ISample>('SELECT * FROM sample');
  },

  async getOneById (id: number): Promise<ISample | undefined> {
    return await getOneFromTableById<ISample>('sample', id);
  },

  async updateOne (
    id: number,
    update: ISampleDTO,
  ): Promise<ISample | undefined> {
    return await updateInTableById<ISample>('sample', id, update);
  },

  async deleteOne (id: number): Promise<number | undefined> {
    const record = await this.getOneById(id);
    if (record === undefined) {
      return undefined;
    }
    await deleteInTableById('sample', id);
    return id;
  },
};

export default sampleModel;
