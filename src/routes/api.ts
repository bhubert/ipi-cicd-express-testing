import express from 'express';
import samplesRouter from './samples';

const apiRouter = express.Router();
apiRouter.use('/samples', samplesRouter);

export default apiRouter;
