import { queryAsync } from '../../src/db';
import sampleModel from '../../src/models/sample';

async function resetSampleTable() {
  await queryAsync('DELETE FROM sample');
  await queryAsync("UPDATE sqlite_sequence SET seq = 0 WHERE name = 'sample'");
}

describe('Sample tests', () => {
  // Réinitialise la table sample avant chaque test
  beforeEach(async () => {
    await resetSampleTable();
  });

  it('Create sample', async () => {
    const sampleRecord = await sampleModel.create({
      name: `Sample sample ${Date.now()}`,
    });
    expect(sampleRecord).toEqual(
      expect.objectContaining({
        id: expect.any(Number),
        name: expect.stringMatching(/^Sample sample \d+$/),
      })
    );
  });

  it('Gets samples', async () => {
    // Arrange: on insère deux enregistrements dans la table
    // pour pouvoir vérifier ensuite qu'on les récupère bien
    await Promise.all([
      sampleModel.create({ name: 'sample1' }),
      sampleModel.create({ name: 'sample2' }),
    ]);

    // Act: on récupère les enregistrements depuis la table
    const records = await sampleModel.getAll();

    // Assert
    // on vérifie qu'on récupère le bon nombre d'enregistrements
    expect(records).toHaveLength(2);
    expect(records).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ id: expect.any(Number), name: 'sample1' }),
        expect.objectContaining({ id: expect.any(Number), name: 'sample2' }),
      ])
    );
  });

  describe('getOneById', () => {
    it('Gets an existing sample', async () => {
      const sample = await sampleModel.create({ name: 'sample' });
      const record = await sampleModel.getOneById(sample.id);
      expect(record).toEqual(sample);
    });

    it('Gets a non-existent sample', async () => {
      const record = await sampleModel.getOneById(1111111);
      expect(record).toBeUndefined();
    });
  });

  describe('updateOne', () => {
    it('Updates a sample', async () => {
      const sampleToUpdate = await sampleModel.create({ name: 'Initial' });

      await sampleModel.updateOne(sampleToUpdate.id, { name: 'Updated' });
      const updatedRecord = await sampleModel.getOneById(sampleToUpdate.id);
      expect(updatedRecord).toHaveProperty('id', sampleToUpdate.id);
      expect(updatedRecord).toHaveProperty('name', 'Updated');
    });

    it('Updates nothing if wrong id', async () => {
      const actual = await sampleModel.updateOne(123456, { name: 'Updated' });
      expect(actual).toBeUndefined();
    });
  });

  describe('deleteOne', () => {
    it('Deletes a sample', async () => {
      // Arrange - insérer un sample pour pouvoir le supprimer ensuite
      const sampleToDelete = await sampleModel.create({ name: 'To be deleted' });

      // Act - supprimer le sample. La méthode va renvoyer l'id du sample effacé.
      const id = await sampleModel.deleteOne(sampleToDelete.id);

      // Assert
      // L'id renvoyé est le même que celui du sample créé auparavant
      expect(id).toBe(sampleToDelete.id)
      // Si on essaie de récupérer le sample, on récupère undefined
      const deletedRecord = await sampleModel.getOneById(sampleToDelete.id);
      expect(deletedRecord).toBeUndefined();
    });

    it('Deletes nothing if wrong id', async () => {
      const id = await sampleModel.deleteOne(123456);
      expect(id).toBeUndefined();
    });
  });
});
